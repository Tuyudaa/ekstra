<div class="container-fluid">
    <h3><?= $data['title'] ?></h3>
    <div class="row">
        <div class="col-lg-8">
        <div class="card">
        <form action="<?= isset($data['data']) ? BASE_URL . '/siswa/update' : BASE_URL . '/siswa/store'?>" enctype="multipart/form-data" method="POST">
            <div class="card-body">
                <?php if(isset($data['data'])) { ?>
                    <input type="hidden" name="id" value="<?= $data['data']['nis']?>">
                <?php } ?>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="nis"><span class="text-danger">*</span> Nis </label>
                            <input type="text"  id="nis" name="nis" placeholder="Nis" class="form-control" required
                            value="<?= isset($data['data']) ? $data['data']['nis'] : ''?>">
                        </div>
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group">
                            <label for="ekstra_id"><span class="text-danger">*</span> Ekstra</label>
                            <select name="ekskul_id" id="ekstra_id" class="form-control">
                                <option value="">Choose Ekstra</option>
                                <?php if(isset($data['data'])) { ?>
                                    <?php foreach($data['data_ekstra'] as $value) { ?>
                                        <option value="<?= $value['id']?>" <?= $data['data']['ekskul_id'] == $value['id'] ? 'selected' : ''?>><?= $value['nama_ekstra']?></option> 
                                    <?php } ?> 
                                <?php } else { ?>
                                    <?php foreach($data['data_ekstra'] as $value) { ?>
                                        <option value="<?= $value['id']?>"><?= $value['nama_ekstra']?></option>
                                    <?php } ?>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-lg-6">
                        <div class="form-group">
                            <label for="nama"><span class="text-danger">*</span> Nama </label>
                            <input type="text" class="form-control" required name="nama" placeholder="Nama" id="nama"
                            value="<?= isset($data['data']) ? $data['data']['nama'] : ''?>">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="jurusan"><span class="text-danger">*</span> Jurusan </label>
                            <input type="text" class="form-control" required id="jurusan" name="jurusan" placeholder="Jurusan"
                            value="<?= isset($data['data']) ? $data['data']['jurusan'] : ''?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                    <div class="form-group">
                            <label for="kelas"><span class="text-danger">*</span> Kelas</label>
                            <input type="text" class="form-control" id="kelas" placeholder="Kelas" name="kelas" required
                            value="<?= isset($data['data']) ? $data['data']['kelas'] : ''?>">
                        </div>
                    </div>
                    <?php if(!isset($data['data'])) { ?>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="password"><span class="text-danger">*</span> Passowrd </label>
                                <input type="password" class="form-control" required name="password" placeholder="Password" id="password"
                                value="">
                            </div>
                        </div>
                    <?php } ?>
                </div>
                
                <div class=" w25 d-flex flex-row">
                    <a href="<?= BASE_URL?>/siswa" class="btn btn-danger mr-2">Back</a>
                    <button type="submit" class="btn btn-success" >Save</button>
                </div>
            </div>
        </form>
    </div>
        </div>
    </div>
</div>