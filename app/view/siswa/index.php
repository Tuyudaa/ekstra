<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Data Siswa</h1>
        <p class="mb-4">Table Data Ekstra siswa/siswi SMK Negri 1 Denpasar.</p>
        <?php Flasher::flash() ?>

        <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header d-flex justify-content-between py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tabel Data Siswa</h6>
            <a href="<?= BASE_URL ?>/siswa/create" class="btn btn-primary">Add</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th><center>NIS</center></th>
                            <th><center>Nama</center></th>
                            <th><center>Jurusan</center></th>
                            <th><center>Kelas</center></th>
                            <th><center>Ekstra</center></th>
                            <th><center>Aksi</center></th>
                        </tr>
                    </thead>
                    <tbody>
                            <?php if(count($data['getAll']) >0) {?>
                            <?php foreach($data['getAll'] as $data) { ?>
                            <tr>
                                <td><?= $data['nis'] ?></td>
                                <td><?= $data['nama'] ?></td>
                                <td><?= $data['jurusan'] ?></td>
                                <td><?= $data['kelas'] ?></td>
                                <td><?= $data['nama_ekstra'] ?></td>
                                <td>
                                    <div>
                                        <a href="<?= BASE_URL?>/siswa/edit/<?= $data['nis']?>" class="btn btn-light"><i class="fa fa-pen"></i></a>
                                        <button  data-toggle="modal" data-target="#logoutModal" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php } else{?>
                            <tr>
                                <td colspan="5" class="text-center">Data not Found</td>
                            </tr>
                        <?php }?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"    
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Yakin ingin menghapus data ini?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <h5>Data yang dihapus tidak bisa dikembalikan!!!</h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
                    <a href="<?= BASE_URL?>/siswa/delete/<?= $data['nis']?>" class="btn btn-success">Hapus</a>
                </div>
            </div>
        </div>
</div>