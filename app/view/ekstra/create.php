<div class="container-fluid">
    <h3><?= $data['title'] ?></h3>
    <div class="row">
        <div class="col-lg-8">
        <?php Flasher::flash() ?>
        <div class="card">
        <form action="<?= isset($data['getSingle']) ? BASE_URL . '/ekstra/update' : BASE_URL . '/ekstra/store'?>" enctype="multipart/form-data" method="POST">
            <div class="card-body">
        <input type="hidden" name="id" value="<?= $data['getSingle']['id']?>">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="nama_ekstra"><span class="text-danger">*</span> Nama ekstra</label>
                            <input type="text"  id="nama_ekstra" name="nama_ekstra" placeholder="Nama Ekstra" class="form-control" required value="<?= isset($data['getSingle']) ? $data['getSingle']['nama_ekstra'] : ''?>">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="penangung_jawab"><span class="text-danger">*</span> Penanggung Jawab</label>
                            <input type="text" class="form-control" required name="penanggung_jawab" placeholder="Penangung Jawab" id="penanggung_jawab" value="<?= isset($data['getSingle']) ? $data['getSingle']['penanggung_jawab'] : ''?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="lokasi_ekstra"><span class="text-danger">*</span> Lokasi Ekstra</label>
                            <input type="text" class="form-control" required id="lokasi_ekstra" name="lokasi_ekstra" placeholder="Lokasi Ekstra" value="<?= isset($data['getSingle']) ? $data['getSingle']['lokasi_ekstra'] : ''?>">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="jadwal_ekstra"><span class="text-danger">*</span> Jadwal ekstra</label>
                            <input type="text" class="form-control" id="jadwal_ekstra" placeholder="Jadwal Ekstra" name="jadwal_ekstra" required value="<?= isset($data['getSingle']) ? $data['getSingle']['jadwal_ekstra'] : ''?>">
                        </div>
                    </div>
                </div>
                <div class=" w25 d-flex flex-row">
                    <a href="<?= BASE_URL?>/ekstra" class="btn btn-danger mr-2">Back</a>
                    <button type="submit" class="btn btn-success" >Save</button>
                </div>
            </div>
        </form>
    </div>
        </div>
    </div>
</div>