<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Data Ekstrakurikuler</h1>
<p class="mb-4">Table Data Ekstrakurikuler siswa/siswi SMK Negri 1 Denpasar.</p>
<?php Flasher::flash() ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header d-flex justify-content-between py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tabel Data Ekstrakurikuler</h6>
        <a href="<?= BASE_URL ?>/ekstra/create" class="btn btn-primary">Add</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th><center>Nama Ekstrakurikuler</center></th>
                        <th><center>Penanggung Jawab</center></th>
                        <th><center>Lokasi Ekskul</center></th>
                        <th><center>Jadwal Ekskul</center></th>
                        <th><center>Opsi</center></th>
                    </tr>
                </thead>
               <tbody>
                    <?php if(count($data['getAll']) >0) {?>
                        <?php foreach($data['getAll'] as $data) { ?>
                        <tr>
                            <td><?= $data['nama_ekstra'] ?></td>
                            <td><?= $data['penanggung_jawab'] ?></td>
                            <td><?= $data['lokasi_ekstra'] ?></td>
                            <td><?= $data['jadwal_ekstra'] ?></td>
                            <td>
                                <div>
                                    <a href="<?= BASE_URL?>/ekstra/edit/<?= $data['id']?>" class="btn btn-light"><i class="fa fa-pen"></i></a>
                                    <button  data-toggle="modal" data-target="#logoutModal" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php } else{?>
                        <tr>
                            <td colspan="5" class="text-center">Data not Found</td>
                        </tr>
                    <?php }?>
               </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"    
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Yakin ingin menghapus data ini?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <h5>Data yang dihapus tidak bisa dikembalikan!!!</h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
                    <a href="<?= BASE_URL?>/ekstra/delete/<?= $data['id']?>" class="btn btn-success">Hapus</a>
                </div>
            </div>
        </div>
    </div>
</div>