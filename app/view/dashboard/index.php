<div class="container-fluid">
    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">Dashboard</h3>

            <p class="panel-subtitle">Selamat Datang Administrator</p>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="card mb-4 py-3 border-left-primary">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <p class="d-flex flex-column">
                                        <span class="number"><?= count($data['countSiswa']) ?></span>
                                        <span class="title">Total Siswa</span>
                                    </p>
                                </div>
                                <div class="col-auto">
                                    <i class="fa fa-users"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card mb-4 py-3 border-left-primary">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <p class="d-flex flex-column">
                                        <span class="number"><?= count($data['countSiswa']) ?></span>
                                        <span class="title">Total Akun</span>
                                    </p>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-user"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card mb-4 py-3 border-left-primary">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <p class="d-flex flex-column">
                                        <span class="number"><?= count($data['countEkstrakurikuler']) ?></span>
                                        <span class="title">Total Ekstrakurikuler</span>
                                    </p>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-list"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card mb-4 py-3 border-left-primary">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <p class="d-flex flex-column">
                                        <span class="number"><?= count($data['countSiswa']) ?></span>
                                        <span class="title">Total Pendaftaran</span>
                                    </p>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-list"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>