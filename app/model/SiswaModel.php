<?php 

class SiswaModel {
    private $table = 'siswa',
            $db;

    public function __construct() {
        $this->db = new Database;
    }

    public function getAll() {
        $this->db->query("SELECT * FROM {$this->table} INNER JOIN ekskul on siswa.ekskul_id = ekskul.id");
        $this->db->execute();
        return $this->db->resultAll();
    }

    public function store($data) {
        $this->db->query("INSERT INTO {$this->table} (`nis`, `nama`, `jurusan`, `kelas`,`password`,`ekskul_id`) VALUES (:nis, :nama, :jurusan, :kelas, :password, :ekskul_id)");
        $this->db->bind('nis', $data['nis']);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('jurusan', $data['jurusan']);
        $this->db->bind('password', md5($data['password']));
        $this->db->bind('kelas', $data['kelas']);
        $this->db->bind('ekskul_id', $data['ekskul_id']);

        $this->db->execute();
        return $this->db->rowCount();
    }
    public function getSingle($nis) {
        $this->db->query("SELECT siswa.nis, siswa.nama, siswa.jurusan, siswa.kelas, ekskul.id as ekskul_id FROM siswa, ekskul WHERE ekskul.id=siswa.ekskul_id AND siswa.nis=:nis");
        $this->db->bind('nis', $nis);
        $this->db->execute();
        return $this->db->resultSingle();
    }
    public function getEkstra()
    {
        $this->db->query("SELECT * FROM ekskul");
        $this->db->execute();
        return $this->db->resultAll();
    }
    public function updateSiswa($data)
    {
        $this->db->query("UPDATE {$this->table} SET nama=:nama, jurusan=:jurusan, ekskul_id=:ekskul_id, kelas=:kelas WHERE nis=:nis");
        $this->db->bind('nis', $data['nis']);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('jurusan', $data['jurusan']);
        $this->db->bind('ekskul_id', $data['ekskul_id']);
        $this->db->bind('kelas', $data['kelas']);
        $this->db->execute();
        
        return $this->db->rowCount();
    }
    public function deleteSiswa($nis)
    {
        $this->db->query("DELETE FROM {$this->table} WHERE nis=:nis");
        $this->db->bind('nis', $nis);
        $this->db->execute();
        return $this->db->rowCount();
    }
}