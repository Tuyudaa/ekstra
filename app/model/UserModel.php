<?php
class UserModel {
    private $table = 'user';
    private $db;

    private $name = 'utulney';
    public function getUser() {
        return $this->name;
    }

    public function __construct() {
        $this->db = new Database;
    }

    public function getAllUser() {
        $this->db->query("SELECT * FROM {$this->table}");
        return $this->db->resultAll();
    }

    public function getUserById($id) {
        $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
        $this->db->bind("id", $id);

        return $this->db->resultSingle();
    }

    public function createUser($data) {
        $query = "INSERT INTO {$this->table} (`username`, `first_name`, `last_name`, `email`, `password`) VALUES (:username, :first_name, :last_name, :email, :password)";
        $this->db->query($query);
        $this->db->bind('username' , $data['username']);
        $this->db->bind('first_name' , $data['first_name']);
        $this->db->bind('last_name' , $data['last_name']);
        $this->db->bind('email' , $data['email']);
        $this->db->bind('password' , md5($data['password']));

        $this->db->execute();
        return $this->db->rowCount();
    }

    public function login($data) {
       $this->db->query("SELECT * FROM  {$this->table} WHERE email=:email");

       $this->db->bind('email', htmlspecialchars($data['email']));
        $this->db->execute();
        $row = $this->db->resultSingle();
 
        if($row != false) {
            $email_db = $row['email'];
            $password_db = $row['password'];
            if($data['email'] == $email_db && $password_db == md5($data['password'])) {
                $_SESSION["login"]= true;
                return true;
            }
            else {
                return false;
            }
        }
    }

    public function register($data) {
        $this->db->query("SELECT * FROM {$this->table} WHERE email=:email");

        $this->db->bind('email', $data['email']);

        $this->db->execute();
        $emailUnique = $this->db->resultSingle();

        if($emailUnique !== false) {
            return 0;
        } else{
            return $this->createUser($data);
        }
    }
}