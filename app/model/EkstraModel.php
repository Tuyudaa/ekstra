<?php 

class EkstraModel {
    private $table = 'ekskul',
            $db;

    public function __construct() {
        $this->db = new Database;
    }

    public function getAll() {
        $this->db->query("SELECT * FROM {$this->table}");
        $this->db->execute();
        return $this->db->resultAll();
    }

    public function store($data) {
        $this->db->query("INSERT INTO {$this->table} (`nama_ekstra`, `penanggung_jawab`, `lokasi_ekstra`, `jadwal_ekstra`) VALUES (:nama_ekstra, :penanggung_jawab, :lokasi_ekstra, :jadwal_ekstra)");
        $this->db->bind('nama_ekstra', $data['nama_ekstra']);
        $this->db->bind('penanggung_jawab', $data['penanggung_jawab']);
        $this->db->bind('lokasi_ekstra', $data['lokasi_ekstra']);
        $this->db->bind('jadwal_ekstra', $data['jadwal_ekstra']);
        $this->db->execute();
        return $this->db->rowCount();
    }
    public function getSingle($id) {
        $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->resultSingle();
    }
    public function deleteEkstra($id)
    {
        $this->db->query("DELETE FROM {$this->table} WHERE id=:id");
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }
    public function updateEkstra($data)
    {   
        $this->db->query("UPDATE {$this->table} SET nama_ekstra=:nama_ekstra, penanggung_jawab=:penanggung_jawab, lokasi_ekstra=:lokasi_ekstra, jadwal_ekstra=:jadwal_ekstra WHERE id=:id");
        $this->db->bind('id', $data['id']);
        $this->db->bind('nama_ekstra', $data['nama_ekstra']);
        $this->db->bind('penanggung_jawab', $data['penanggung_jawab']);
        $this->db->bind('lokasi_ekstra', $data['lokasi_ekstra']);
        $this->db->bind('jadwal_ekstra', $data['jadwal_ekstra']);
        $this->db->execute();
        
        return $this->db->rowCount();
    }
}   