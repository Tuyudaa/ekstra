<?php 

class Ekstra extends Controller {
    public function index() {
        $data['getAll'] = $this->model('EkstraModel')->getAll();
        $data['title'] = 'Ekstra';
        $this->view('templates/header',$data);
        $this->view('ekstra/index', $data);
        $this->view('templates/footer');
    }

    public function create() {
        $data['title'] = 'Add Ekstra';
        $this->view('templates/header', $data);
        $this->view('ekstra/create', $data);
        $this->view('templates/footer');
    }

    public function store() {
        if($this->model('EkstraModel')->store($_POST) > 0) {
            Flasher::setFlash('berhasil','menambahkan data Ekstra','success');
            return header('Location: '. BASE_URL . '/ekstra');
            exit;
        } else {
            Flasher::setFlash('gagal','menambahkan data Ekstra','error');
            return header('Location: '. BASE_URL . '/ekstra/create');
            exit;
        }
    }

    public function edit($id) 
    {
        $data['title'] = 'Edit Ekstra';
        $data['getSingle'] = $this->model('EkstraModel')->getSingle($id);
        $this->view('templates/header', $data);
        $this->view('ekstra/create', $data);
        $this->view('templates/footer');
    }
    public function delete($id) 
    {
        if($this->model('EkstraModel')->deleteEkstra($id) >0) 
        {
            Flasher::setFlash('berhasil', 'menghapus data Ekstra', 'success');
            return header("Location:" .BASE_URL. '/ekstra');
            exit;
        } else 
        {
            Flasher::setFlash('gagal', 'menghapus data Ekstra', 'danger');
            return header("Location:" .BASE_URL. '/ekstra');
            exit;
        }
    }
    public function update()
    {
        if($this->model('EkstraModel')->updateEkstra($_POST) > 0)
        {
            Flasher::setFlash('berhasil','mengedit data Ekstra','success');
            return header("Location:" .BASE_URL. '/ekstra');
            exit;
        }else
        {
            Flasher::setFlash('gagal', 'mengedit data Ekstra', 'danger');
            return header("Location:" .BASE_URL. '/ekstra');
            exit;
        }
    }
}