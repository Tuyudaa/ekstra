<?php
class Auth extends Controller{
    public function login()
    {   
        $this->view('auth/login');
    }

    public function register()
    {   
        $this->view('auth/register');
    }

    public function loginpost()
    {
        if(isset($_SESSION["login"])) {
            Flasher::setFlash('Sudah mempunyai akun','login','success');
            return header('Location: '. BASE_URL . '/');
        }
        if($this->model('UserModel')->login($_POST)) {
            Flasher::setFlash('berhasil','login','success');
            return header('Location: ' . BASE_URL .'/');
        } else{
            Flasher::setFlash('gagal','login','danger');
            $data['msg'] = "Ada kesalahan";
            $this->view('auth/login', $data);
        }
    }
    

    public function registerpost()
    {
        if(isset($_SESSION["login"])) {
            return header('Location: '. BASE_URL . '/');
        }
        if($this->model('UserModel')->register($_POST) > 0) {
            return header('Location: ' . BASE_URL .'/auth/login');
        } else{
            return header('Location: ' . BASE_URL .'/auth/login');
        }
    }

    public function logout()
    {
        if(!isset($_SESSION["login"])) {
            return header('Location: '. BASE_URL . '/auth/login');
        }
        session_destroy();
        return header('Location: ' . BASE_URL . '/auth/login');
    }
}
?>

