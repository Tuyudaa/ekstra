<?php 

class Dashboard extends Controller {
    public function index()
    {
        $data['title']= 'Dashboard';
        $data['countSiswa'] = $this->model('SiswaModel')->getAll();
        $data['countAkun'] = $this->model('SiswaModel')->getAll();
        $data['countEkstrakurikuler'] = $this->model('EkstraModel')->getAll();
        $data['countPendaftaran'] = $this->model('SiswaModel')->getAll();
        $this->view('templates/header', $data);
        $this->view('dashboard/index', $data);
        $this->view('templates/footer');
    }
}