<?php 

class Siswa extends Controller {
    public function index()
    {
        $data['title'] = 'Siswa';
        $data['getAll'] = $this->model('SIswaModel')->getAll();
        $this->view('templates/header', $data);
        $this->view('siswa/index', $data);
        $this->view('templates/footer');
    }
    public function create() {
        $data['title'] = 'Add Siswa';
        $data['data_ekstra'] = $this->model('SiswaModel')->getEkstra();
        $this->view('templates/header', $data);
        $this->view('siswa/create', $data);
        $this->view('templates/footer');
    }
    public function store() {
        if($this->model('SiswaModel')->store($_POST) > 0) {
            Flasher::setFlash('berhasil','menambahkan data Siswa','success');
            return header('Location: '. BASE_URL . '/siswa');
            exit;
        } else {
            Flasher::setFlash('gagal','menambahkan data Siswa','danger');
            return header('Location: '. BASE_URL . '/siswa/create');
            exit;
        }
    }
    public function edit($nis) 
    {
        $data['title'] = 'Edit Siswa';
        $data['data'] = $this->model('SiswaModel')->getSingle($nis);
        $data['data_ekstra'] = $this->model('SiswaModel')->getEkstra();
        $this->view('templates/header', $data);
        $this->view('siswa/create', $data);
        $this->view('templates/footer');
    }
    public function update()
    {
        if($this->model('SiswaModel')->updateSiswa($_POST) > 0)
        {
            Flasher::setFlash('berhasil','mengedit data Siswa','success');
            return header("Location:" .BASE_URL. '/siswa');
            exit;
        }else
        {
            Flasher::setFlash('gagal', 'mengedit data Siswa', 'danger');
            return header("Location:" .BASE_URL. '/siswa');
            exit;
        }
    }
    public function delete($nis) 
    {
        if($this->model('SiswaModel')->deleteSiswa($nis) >0) 
        {
            Flasher::setFlash('berhasil', 'menghapus data Siswa', 'success');
            return header("Location:" .BASE_URL. '/siswa');
            exit;
        } else 
        {
            Flasher::setFlash('gagal', 'menghapus data Siswa', 'danger');
            return header("Location:" .BASE_URL. '/siswa');
            exit;
        }
    }
}